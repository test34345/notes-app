import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Route, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import HeaderComponent from './layout/header/header.component';
import AddNoteComponent from './notes/add-note/add-note.component';
import FilterNotesComponent from './notes/filter-notes/filter-notes.component';
import NotesComponent from './notes/notes.component';
import SingleNoteComponent from './notes/single-note/single-note.component';
import FilterNotesPage from './pages/filter-notes-page/filter-notes-page.component';
import HomePageComponent from './pages/home-page/home-page.component';
import NewNotePageComponent from './pages/new-note-page/new-note-page.component';
import NotesService from './shared/notes.service';
import ScaleOnHoverDirective from './shared/scale-on-hover.directive';
import CardComponent from './smaller-components/card/card.component';

const appRoutes: Route[] = [
  { path: '', component: HomePageComponent },
  { path: 'add-note', component: NewNotePageComponent },
  { path: 'filter-notes', component: FilterNotesPage },
];

@NgModule({
  declarations: [
    AppComponent,
    NotesComponent,
    AddNoteComponent,
    HeaderComponent,
    SingleNoteComponent,
    CardComponent,
    HomePageComponent,
    NewNotePageComponent,
    FilterNotesPage,
    FilterNotesComponent,
    ScaleOnHoverDirective,
  ],
  imports: [BrowserModule, FormsModule, RouterModule.forRoot(appRoutes)],
  providers: [NotesService],
  bootstrap: [AppComponent],
})
export class AppModule {}
