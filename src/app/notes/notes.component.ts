import { Component, OnInit } from '@angular/core';
import NotesService from '../shared/notes.service';
import Note from './note.model';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export default class NotesComponent implements OnInit {
  notes: Note[];

  constructor(private notesService: NotesService) {}

  ngOnInit() {
    this.notes = this.notesService.getNotes();
  }
}
