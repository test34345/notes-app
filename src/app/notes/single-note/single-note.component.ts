import { Component, Input } from '@angular/core';
import NotesService from 'src/app/shared/notes.service';
import Note from '../note.model';

@Component({
  selector: 'app-single-note',
  templateUrl: './single-note.component.html',
  styleUrls: ['./single-note.component.scss'],
})
export default class SingleNoteComponent {
  @Input() note: Note;

  constructor(private notesService: NotesService) {}

  onNoteClick() {
    console.log(1);
    this.notesService.removeNote(this.note.id);
  }
}
