import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import Note from '../note.model';
import NotesService from 'src/app/shared/notes.service';

type InputType = HTMLInputElement | HTMLTextAreaElement;

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
})
export default class AddNoteComponent {
  @ViewChild('titleInput') titleInputElRef: ElementRef<HTMLInputElement>;
  @ViewChild('descriptionInput')
  descriptionInputElRef: ElementRef<HTMLTextAreaElement>;

  constructor(private notesService: NotesService) {}

  private getInputValue(elementRef: ElementRef<InputType>) {
    return elementRef.nativeElement.value;
  }

  onAddNote() {
    const title = this.getInputValue(this.titleInputElRef);
    const description = this.getInputValue(this.descriptionInputElRef);
    const noteDate = new Date();
    const newNote = new Note({ title, description, date: noteDate });

    // add to the notes service array
    this.notesService.addNote(newNote);
  }
}
