interface NoteConfig {
  title: string;
  description: string;
  date: Date;
}

export default class Note {
  public title: string;
  public description: string;
  public date: Date;
  public id: number;

  constructor(config: NoteConfig) {
    this.title = config.title;
    this.description = config.description;
    this.date = config.date;
    this.id = Math.random() * 3214321 + Math.random() * 32;
  }
}
