import { Component, OnInit, ViewChild } from '@angular/core';
import NotesService from 'src/app/shared/notes.service';
import Note from '../note.model';

@Component({
  selector: 'app-filter-notes',
  templateUrl: './filter-notes.component.html',
})
export default class FilterNotesComponent implements OnInit {
  filteredNotes: Note[];

  constructor(private notesService: NotesService) {}

  ngOnInit() {
    this.filteredNotes = this.notesService.getNotes();

    this.notesService.onNotesChanged.subscribe((newNotes) => {
      this.filteredNotes = newNotes;
    });
  }

  onFilterInput(event: Event) {
    const input = event.target as HTMLInputElement;
    const filterValue = input.value;

    const filteredNotes = this.notesService.getfilteredNotes(filterValue);

    this.notesService.onNotesChanged.emit(filteredNotes);
  }
}
