import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[scaleOnHover]',
})
export default class ScaleOnHoverDirective {
  @HostBinding() scaleOnHover: string = '1.2';

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {}

  private setStyle(stylePropName: string, value: string) {
    this.renderer.setStyle(this.elementRef.nativeElement, stylePropName, value);
  }

  @HostListener('mouseover')
  onMouseOver() {
    this.setStyle('transform', `scale(${this.scaleOnHover})`);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.setStyle('transform', 'initial');
  }
}
