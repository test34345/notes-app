import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Note from '../notes/note.model';

@Injectable()
export default class NotesService {
  public onNotesChanged = new EventEmitter<Note[]>();

  private notes: Note[] = [
    new Note({ title: 'Note 1', description: 'Note 1 desc', date: new Date() }),
    new Note({ title: 'Note 2', description: 'Note 2 desc', date: new Date() }),
    new Note({ title: 'Note 3', description: 'Note 3 desc', date: new Date() }),
  ];

  constructor(private router: Router) {}

  getNotes() {
    return [...this.notes];
  }

  getfilteredNotes(filterText: string) {
    return this.notes.filter((curNote) =>
      curNote.title.toLowerCase().includes(filterText.toLowerCase())
    );
  }

  addNote(newNote: Note) {
    this.notes.push(newNote);
    this.router.navigate(['']);
  }

  removeNote(noteId: number) {
    const userConfirmsDelete = confirm(
      'Are you sure you want to delete this note?'
    );

    console.log(userConfirmsDelete);

    if (!userConfirmsDelete) return;

    const noteIndex = this.notes.findIndex((curNote) => curNote.id === noteId);

    if (noteIndex === -1) return;

    this.notes.splice(noteIndex, 1);
    this.onNotesChanged.emit([...this.notes]);
  }
}
